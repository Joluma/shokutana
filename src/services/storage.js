import shortid from 'shortid';

let db;

function setupDB() {
  return new Promise((resolve, reject) => {
    // If setupDB has already been run and the database was set up, no need to
    // open the database again; just resolve and return!
    if (db) {
      return resolve();
    }

    const dbReq = indexedDB.open('shokutana', 2);

    // Fires when the version of the database goes up, or the database is
    // created for the first time
    dbReq.onupgradeneeded = event => {
      console.log('%c==> onupgradeneeded', 'font-size:xx-large;color:orange', );
      db = event.target.result;

      // Create an object store named products, or retrieve it if it already
      // exists. Object stores in databases are where data are stored.
      let groupsStore,
          productsStore;

      if (!db.objectStoreNames.contains('groups') || !db.objectStoreNames.contains('products')) {
        groupsStore = db.createObjectStore('groups', {autoIncrement: true});
        productsStore = db.createObjectStore('products', {autoIncrement: true});
      } else {
        groupsStore = dbReq.transaction.objectStore('groups');
        productsStore = dbReq.transaction.objectStore('products');
      }

      // If there isn't already a _id index, make one so we
      // can query products by their _ids
      if (!groupsStore.indexNames.contains('_id')) {
        groupsStore.createIndex('_id', '_id', {unique: true});
      }

      if (!groupsStore.indexNames.contains('name')) {
        groupsStore.createIndex('name', 'name', {unique: true});
      }

      if (!productsStore.indexNames.contains('_id')) {
        productsStore.createIndex('_id', '_id', {unique: true});
      }

      if (!productsStore.indexNames.contains('_groupId')) {
        productsStore.createIndex('_groupId', '_groupId');
      }
    }

    // Fires once the database is opened (and onupgradeneeded completes, if
    // onupgradeneeded was called)
    dbReq.onsuccess = event => {
      // Set the db variable to our database so we can use it!
      db = event.target.result;
      resolve();
    }

    // Fires when we can't open the database
    dbReq.onerror = event => {
      reject(`Error opening database ${event.target.error.message}`);
    }
  });
}

function getAll(collection) {
  return new Promise((resolve, reject) => {
    let transaction = db.transaction([collection], 'readonly');

    let store = transaction.objectStore(collection);
    let req = store.openCursor(null, 'next');

    let allCollection = [];

    req.onsuccess = (event) => {
      let cursor = event.target.result;

      if (cursor != null) {
        allCollection.push(cursor.value);
        cursor.continue();
      } else {
        resolve(allCollection);
      }
    }

    req.onerror = (event) => {
      reject(`Error in cursor request ${event.target.error.message}`);
    }
  });
}

function getByKey(collection, { key, value }) {
  return new Promise((resolve, reject) => {
    let transaction = db.transaction([collection], 'readwrite');

    let store = transaction.objectStore(collection);
    let index = store.index(key);
    const keyRange = IDBKeyRange.only(value);
    const req = index.openCursor(keyRange);

    let products = [];

    req.onsuccess = (event) => {
      let cursor = event.target.result;

      if (cursor != null) {
        products.push(cursor.value);
        cursor.continue();
      } else {
        resolve(products);
      }
    }

    req.onerror = (event) => {
      reject(`error while updating ${product.name}: ${event.target.error.message}`);
    }
  });
}

function remove(collection, item) {
  return new Promise((resolve, reject) => {
    let transaction = db.transaction([collection], 'readwrite');

    let store = transaction.objectStore(collection);
    let index = store.index('_id');
    const keyRange = IDBKeyRange.only(item._id);
    const req = index.openCursor(keyRange);

    req.onsuccess = (event) => {
      let itemToDelete = event.target.result;
      const requestDelete = itemToDelete.delete(item);

      requestDelete.onsuccess = event => {
        resolve({ success: true })
      };

      requestDelete.onerror = event => {
        reject(`error while deleting ${itemToDelete.name}: ${event.target.error.message}`);
      };
    }

    req.onerror = (event) => {
      reject(`error while deleting ${item.name}: ${event.target.error.message}`);
    }
  });
}

function save(collection, item) {
  return new Promise((resolve, reject) => {
    let transaction = db.transaction([collection], 'readwrite');
    let store = transaction.objectStore(collection);

    item._id = shortid.generate();
    store.add(item);

    transaction.oncomplete = resolve;

    transaction.onerror = function(event) {
      reject(`error storing item ${event.target.error.message}`);
    }
  });
}

function update(collection, item) {
  return new Promise((resolve, reject) => {
    let transaction = db.transaction([collection], 'readwrite');

    let store = transaction.objectStore(collection);
    let index = store.index('_id');
    const keyRange = IDBKeyRange.only(item._id);
    const req = index.openCursor(keyRange);

    req.onsuccess = (event) => {
      let updatedItem = event.target.result;
      const requestUpdate = updatedItem.update(item);

      requestUpdate.onsuccess = event => {
        resolve({ success: true })
      };

      requestUpdate.onerror = event => {
        reject(`error while updating ${updatedItem.name}: ${event.target.error.message}`);
      };
    }

    req.onerror = (event) => {
      reject(`error while updating ${item.name}: ${event.target.error.message}`);
    }
  });
}

export { getAll, getByKey, remove, save, setupDB, update };
