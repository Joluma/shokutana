import dayjs from 'dayjs';

import { getAll } from './storage';

const ONE_HOUR_IN_MS = 1000 * 60 * 60;

function areNotificationsSupported() {
  return "Notification" in window;
}

function areNotificationsEnabled() {
  return areNotificationsSupported() && Notification.permission === 'granted';
}

async function checkNotificationPromise() {
  try {
    await Notification.requestPermission();
  } catch(e) {
    return false;
  }

  return true;
}

function handlePermission(permission) {
  if(!('permission' in Notification)) {
    Notification.permission = permission;
  }
}

async function askNotificationPermission() {
  if (!areNotificationsSupported()) return;

  if(checkNotificationPromise()) {
    const permission = await Notification.requestPermission()
    handlePermission(permission);
  } else {
    await Notification.requestPermission(function(permission) {
      handlePermission(permission);
    });
  }
}

async function checkDeadlines() {
  if (!areNotificationsEnabled()) return;

  const now = dayjs();
  const products = await getAll('products');

  products.forEach(product => {
    const daysUntilExpires = dayjs(product.expirationDate).diff(now, 'day', true);

    daysUntilExpires < 1 && createNotification(product, 'passedDate');
    1 < daysUntilExpires && daysUntilExpires < 3 && createNotification(product, 'closeDate');
    3 < daysUntilExpires && daysUntilExpires < 5 && createNotification(product, 'shortDate');
  });
}

function createNotification(product, status) {
  let img = 'images/food_contour.png';
  let text = `
    HEY! You should eat your "${product.name}" before it's too late.
    His expiration date is ${dayjs(product.expirationDate).format('D MMM YYYY')}!
  `;
  let notification = new Notification('食棚', { body: text, icon: img });
}

setInterval(checkDeadlines, ONE_HOUR_IN_MS);

export {
  askNotificationPermission,
  areNotificationsEnabled,
  areNotificationsSupported
};
